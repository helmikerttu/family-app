# Family App

An application for managing family schedules, todos, and shopping lists.
The application consist of [React](https://reactjs.org/) client and [Flask](https://www.palletsprojects.com/p/flask/) server.
The database uses SQLite by default, but the server uses [SQLAlchemy](https://www.sqlalchemy.org/) so it supports most SQL databases.

This project is still a work in progress, but due to agile development the `master` branch should be always functional.
For instance, there is currently no support for multiple families, so all users belong to the same family. 
See Development progress section for the current state of the project.

## Requirements

- [Python](https://www.python.org/) 3.7+
- [Node.js](https://nodejs.org/) 12.13+

## Installation

Run `npm install` in client directory and `pip install -r requirements.txt` in server directory.
For installing the database, it's recommended to use Python REPL: 
```python
>>> from app import db, models
>>> db.create_all()
```

## Usage

Start the client by running `npm start` in the client directory.
Start the server by running `python -m app` in the server directory.
The app will now be running at http://localhost:3000/.

## Docker

You can also run the application using Docker by running `docker-compose build && docker-compose up` in the root directory.

## Development progress

- [x] Authentication
- [x] Database
- [ ] Dashboard
- [ ] Calendar page
- [ ] ToDo board
- [x] Shopping list page
- [x] Settings (change password)
- [ ] Support for multiple families
- [ ] Advanced registration page

## Screenshots

![Login](https://i.imgur.com/Pfqq1ib.png)

![Settings](https://i.imgur.com/LgUvK5a.png)
