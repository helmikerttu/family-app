import datetime

from flask import jsonify
from flask import request
import jwt

from . import bcrypt
from . import db
from . import app
from .models import User
from .models import ShoppingListItem


def parse_auth_token(auth_header):
    auth_token = auth_header.split(' ', maxsplit=1)[1]
    try:
        data = jwt.decode(auth_token, app.config['SECRET_KEY'], algorithms=['HS256'])
    
    except jwt.ExpiredSignatureError:
        return None
    
    return data.get('user')


@app.route('/', methods=['GET'])
def root():
    return jsonify({'message' : 'Hello world!'})


@app.route('/validate', methods=['GET'])
def validate():
    auth_header = request.headers.get('Authorization')
    if not auth_header:
        return jsonify({'message': 'Token missing', 'user': None})

    username = parse_auth_token(auth_header)
    if username:
        return jsonify({'message': 'Logged in', 'user': username})

    else:
        return jsonify({'message': 'Token invalid or expired', 'user': None})


@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    username = data.get('username')
    password = data.get('password')
    user = User.query.filter_by(username=username).one_or_none()

    if user is None:
        return jsonify({'message': 'Unauthorized', 'token': None}), 401

    if not bcrypt.check_password_hash(user.password, password):
        return jsonify({'message': 'Unauthorized', 'token': None}), 401

    now = datetime.datetime.utcnow()
    expiration = now + datetime.timedelta(hours=1)
    token = jwt.encode(
        {
            'user': username,
            'exp': expiration,
        },
        app.config['SECRET_KEY'],
        algorithm='HS256',
    )
    return jsonify({'message': 'Authorized', 'token': token.decode('utf-8')})


@app.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    username = data.get('username')
    password = data.get('password')

    if not username:
        return jsonify({'message': 'Invalid username'})

    if not password:
        return jsonify({'message': 'Invalid password'})

    hashed_password = bcrypt.generate_password_hash(password).decode('utf-8')
    user = User(username=username, password=hashed_password)
    db.session.add(user)
    db.session.commit()
    return jsonify({'message': 'Registered'})


@app.route('/change-password', methods=['POST'])
def change_password():
    auth_header = request.headers.get('Authorization')
    if not auth_header:
        return jsonify({'message': 'Token missing'})

    username = parse_auth_token(auth_header)
    if not username:
        return jsonify({'message': 'Token invalid or expired'})

    data = request.get_json()
    current_password = data.get('current-password')
    new_password = data.get('new-password')

    if not current_password or not new_password:
        return jsonify({'message': 'Missing password'})
    
    user = User.query.filter_by(username=username).one()
    if not bcrypt.check_password_hash(user.password, current_password):
        return jsonify({'message': 'Invalid password'})
    
    hashed_password = bcrypt.generate_password_hash(new_password).decode('utf-8')
    user.password = hashed_password
    db.session.commit()
    return jsonify({'message': 'Password changed'})


@app.route('/shopping-list', methods=['GET'])
def get_shopping_list():
    shopping_list = ShoppingListItem.query.all()
    return jsonify({
        'message': 'Success',
        'data': [item.serialize() for item in shopping_list]
    })


@app.route('/shopping-list', methods=['POST'])
def post_shopping_list():
    data = request.get_json()
    item_texts = data.get('items')
    items = []

    for item_text in item_texts:
        item = ShoppingListItem(text=item_text)
        db.session.add(item)
        items.append(item.serialize())

    db.session.commit()
    return jsonify({'message': 'Items added', 'items': items})


@app.route('/shopping-list', methods=['DELETE'])
def delete_shopping_list():
    data = request.get_json()
    item_ids = data.get('items')

    for item_id in item_ids:
        ShoppingListItem.query.filter_by(id=item_id).delete()
    
    db.session.commit()
    return jsonify({'message': 'Items removed'})


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=3001)