from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_bcrypt import Bcrypt

app = Flask(__name__)

# TODO get configs from environment
app.config['SECRET_KEY'] = 'Temp key'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'

bcrypt = Bcrypt(app)
db = SQLAlchemy(app)
CORS(app)