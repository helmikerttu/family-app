import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute';
import App from './components/App';
import Authentication from './components/Authentication';
import Settings from './components/Settings';
import ShoppingList from './components/ShoppingList';

const router = (
  <Router>
    <Switch>
      <PrivateRoute exact path="/" component={App} />
      <Route path="/login" component={Authentication} />
      <PrivateRoute path="/settings" component={Settings} />
      <PrivateRoute path="/shopping-list" component={ShoppingList} />
    </Switch>
  </Router>
);

ReactDOM.render(router, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
