import Cookies from 'js-cookie';

export async function httpRequest(url, method, data=null) {
  const options = {
    method: method,
    headers: {
      'Content-Type': 'application/json'
    }
  }
  if (data) {
    options.body = JSON.stringify(data);
  }

  const authToken = Cookies.get('Auth token');
  if (authToken) {
    options.headers['Authorization'] = `Bearer ${authToken}`;
  }
  const response = await fetch(url, options);
  return await response.json();
}