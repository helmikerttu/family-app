import React from 'react';

import Header from './Header';
import NavigationBar from './NavigationBar';

function App() {
  return (
    <div>
      <Header />
      <NavigationBar />
    </div>
  );
}

export default App;
