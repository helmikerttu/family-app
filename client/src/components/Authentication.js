import React, { useState } from 'react';
import Cookies from 'js-cookie';
import { httpRequest } from '../helpers';

import Header from './Header';
import './Common.css';

function Authentication(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  async function login() {
    let data = {};
    try {
      data = await httpRequest('http://localhost:3001/login', 'POST', {
        username,
        password
      });
    }
    catch (error) {
      console.log(error);
      return;
    }
    if (data.token) {
      Cookies.set('Auth token', data.token);
      props.history.push('/');
    }
  }

  async function register() {
    try {
      await httpRequest('http://localhost:3001/register', 'POST', {
        username,
        password
      });
    }
    catch (error) {
      console.log(error);
      return;
    }
  }

  return (
    <div className="Authentication">
      <Header />
      <div className="card">
        <p className="card-title font">Log in</p>
        <label for="username" className="label font">Username:</label>
        <input type="text" 
          className="input-field font"
          onChange={(event) => setUsername(event.target.value)} 
          value={username} 
          id="username" />
        <label for="password" className="label font">Password:</label>
        <input type="text" 
          className="input-field font"
          onChange={(event) => setPassword(event.target.value)} 
          value={password} 
          id="password" />
        <button className="input-button font" id="login" onClick={login}>Log in</button>
        <label for="register" className="button-label font">New Family user?</label>
        <button className="input-button font" id="register" onClick={register}>Register</button>
      </div>
    </div>
  );
}

export default Authentication;