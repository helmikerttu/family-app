import React, { useState } from 'react';
import Cookies from 'js-cookie';
import { httpRequest } from '../helpers';

import Header from './Header';
import NavigationBar from './NavigationBar';
import './Common.css';

function Settings({ setUser }) {
  const [currentPassword, setCurrentPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');

  async function changePassword() {
    try {
      await httpRequest('http://localhost:3001/change-password', 'POST', {
        'current-password': currentPassword,
        'new-password': newPassword
      });
    }
    catch (error) {
      console.log(error);
      return;
    }
  }

  async function logout() {
    Cookies.remove('Auth token');
    setUser(null);
  }

  return (
    <div className="Settings">
      <Header />
      <NavigationBar />
      <div className="board">
        <div className="card">
          <p className="card-title font">Change password</p>
          <label for="current-password" className="label font">Current password:</label>
          <input type="text" 
            className="input-field font" 
            onChange={(event) => setCurrentPassword(event.target.value)} 
            id="current-password" />
          <label for="new-password" className="label font">New password:</label>
          <input type="text" 
            className="input-field font"
            onChange={(event) => setNewPassword(event.target.value)} 
            id="new-password" />
          <button className="input-button font" 
            onClick={changePassword}>Change password</button>
        </div>
        <div className="card">
          <p className="card-title font">Log out</p>
          <button className="input-button font" id="logout" onClick={logout}>Log out</button>
        </div>
      </div>
    </div>
  );
}

export default Settings;