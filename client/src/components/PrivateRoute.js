import React, { useState } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAsyncEffect } from 'use-async-effect';
import { httpRequest } from '../helpers';
import Header from './Header';

function PrivateRoute({ component: Component, ...rest }) {
  const [validated, setValidated] = useState(false);
  const [user, setUser] = useState(null);

  // Get current user
  useAsyncEffect(async (isMounted) => {
    const data = await httpRequest('http://localhost:3001/validate', 'GET');
    if (!isMounted()) return;
    setUser(data.user);
    setValidated(true);
  });

  function render(props) {
    if (validated === false) {
      return <Header />;
    }
    if (user !== null) {
      return <Component {...props} user={user} setUser={setUser} />;
    }
    else {
      return <Redirect to="/login" />;
    }
  }

  return <Route {...rest} render={render} />;
}

export default PrivateRoute;