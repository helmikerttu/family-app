import React from 'react';
import { Link } from 'react-router-dom';
import homeIcon from '../img/home.png';
import calendarIcon from '../img/calendar.png';
import todoIcon from '../img/todo.png';
import shopIcon from '../img/shop.png';
import settingsIcon from '../img/settings.png';

import './NavigationBar.css';

function NavigationBar() {
  return (
    <div className="NavigationBar">
      <Link to="/" className="icon-link">
        <img className="icon" src={homeIcon} />
      </Link>
      <Link to="/calendar" className="icon-link">
        <img className="icon" src={calendarIcon} />
      </Link>
      <Link to="/todo-board" className="icon-link">
        <img className="icon" src={todoIcon} />
      </Link>
      <Link to="/shopping-list" className="icon-link">
        <img className="icon" src={shopIcon} />
      </Link>
      <Link to="/settings" className="icon-link">
        <img className="icon" src={settingsIcon} />
      </Link>
    </div>
  );
}

export default NavigationBar;