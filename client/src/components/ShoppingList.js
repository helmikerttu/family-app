import React, { useState } from 'react';
import { useAsyncEffect } from 'use-async-effect';
import { httpRequest } from '../helpers';

import Header from './Header';
import NavigationBar from './NavigationBar';
import './Common.css';
import './ShoppingList.css'

function ShoppingList() {
  const [items, setItems] = useState([]);
  const [newItem, setNewItem] = useState('');

  // Get shopping list from server
  useAsyncEffect(async (isMounted) => {
    const data = await httpRequest('http://localhost:3001/shopping-list', 'GET');
    if (!isMounted()) return;
    setItems(data.data);
  }, []);

  async function addItem() {
    let data = null;
    try {
      data = await httpRequest('http://localhost:3001/shopping-list', 'POST', {'items': [newItem]});
    }
    catch (error) {
      console.log(error);
      return;
    }
    setItems([...items, ...data.items]);
    setNewItem('');
  }

  async function clearItems() {
    const ids = items.map(value => {
      return value.id;
    })
    try {
      await httpRequest('http://localhost:3001/shopping-list', 'DELETE', {'items': ids});
    }
    catch (error) {
      console.log(error);
      return;
    }
    setItems([]);
  }

  return (
    <div className="ShoppingList">
      <Header />
      <NavigationBar />
      <div className="board">
        <div className="card">
          <p className="card-title font">Shopping list</p>
          <label for="new-list-item" className="label font">Add new item to list:</label>
          <input type="text"
            className="input-field font"
            onChange={(event) => setNewItem(event.target.value)} 
            id="new-list-item"
            value={newItem} />
          <button className="add-button font" onClick={addItem}>Add</button>
          <label for="clear-button" className="button-label font">Clear list:</label>
          <button className="clear-button font" id="clear-button" onClick={clearItems}>Clear</button>
        </div>
        <div className="card">
          <div className="list">
            {items.map((value, index) => {
              return <div className="list-item font" key={index}>{value.text}</div>
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

export default ShoppingList;